const Koa = require('koa');
const Router = require('koa-router');
const Pug = require('koa-pug');
const path = require('path');
const bodyParser = require('koa-bodyparser');

const app = new Koa();

app.use(bodyParser());

const pug = new Pug({
  viewPath: './views',
  basedir: './views',
  app,
});

const router = new Router();
const getData = require('./controllers/fodmaps');
const search = require('./controllers/search');

const jsonFile = path.join(__dirname, 'data/food.json');

router.get('/', async (ctx) => {
  const data = await getData.allData(jsonFile);
  const foods = await getData.getSpecificFood(data, 'fruit', 'dairy', 'vegetable');
  ctx.render('index', { foods });
});

router.post('/search', async (ctx) => {
  const food = ctx.request.body.searchFood;
  const searchedFood = await search.findFood(food);
  ctx.render('search', { searchedFood });
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);
