const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);

function filterFood(jsonData, foodWanted) {
  return Object.keys(jsonData)
    .filter(key => foodWanted.includes(key))
    .reduce((obj, key) => {
      obj[key] = jsonData[key];
      return obj;
    }, {});
}

async function allData(path) {
  try {
    return await readFile(path, 'utf8');
  } catch (e) {
    return Promise.reject(e);
  }
}

async function getSpecificFood(data, ...foodWanted) {
  const jsonData = JSON.parse(data);
  try {
    const specificFood = filterFood(jsonData, foodWanted);
    return specificFood;
  } catch (e) {
    return Promise.reject(e);
  }
}

module.exports = {
  allData,
  getSpecificFood,
  filterFood,
};
