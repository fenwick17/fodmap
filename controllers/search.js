const path = require('path');

const getData = require('./fodmaps');

const jsonFile = path.join(__dirname, './../data/food.json');

async function findFood(food) {
  const data = await getData.allData(jsonFile);
  let jsonData = JSON.parse(data);
  jsonData = Object.values(jsonData);
  for (const inner of jsonData) {
    const search = inner.find(f => f.name === food);
    return search;
  }
  return 'Food not found';
}

module.exports = {
  findFood,
};
